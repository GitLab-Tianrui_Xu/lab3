package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    // @Test
    // public void test() {
    //     fail("Not yet implemented");
    // }
    
    @Test
    public void echo_returns_5() {
        App app1 = new App();
        assertEquals("Unit test for echo method", 5, app1.echo(5));
    }

    @Test
    public void oneMore_returns_3() {
        App app1 = new App();
        assertEquals("Unit test for oneMore method", 4, app1.oneMore(3));
    }
}

